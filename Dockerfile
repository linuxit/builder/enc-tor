FROM debian:stable
MAINTAINER admin@itk98.net

RUN apt update && apt upgrade -y && \
    apt install -y git build-essential automake \
    libevent-dev libssl-dev zlib1g-dev \
    libzstd-dev liblzma-dev \
    nyx

WORKDIR /usr/src
RUN git clone https://git.torproject.org/tor.git

WORKDIR /usr/src/tor
RUN git checkout `git ls-remote --tags|grep -v 'alpha\|dev\|rc\|{}'|tail -n 1|awk '{print $1}'`
RUN ./autogen.sh && \
    sed -i 's/ARFLAGS = cru/ARFLAGS = cr/g' Makefile.in
RUN ./configure --prefix=/opt/tor --disable-asciidoc && \
    make && \
    make install

WORKDIR /
RUN rm -rf /usr/src/tor && apt clean all

EXPOSE 80:8080
EXPOSE 443:8443
EXPOSE 9051:9051

ENTRYPOINT ["/opt/tor/bin/tor"]
CMD ["-f", "/etc/tor/torrc"]
